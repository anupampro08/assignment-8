package org.assignment;

public class Interest {
	private double principal;
	private double rate;
	private double time;
	
	public double getPrincipal() {
		return principal;
	}
	public void setPrincipal(double principal) {
		this.principal = principal;
	}
	
	public double getRate() {
		return rate;
	}
	
	public void setRate(double rate) {
		this.rate = rate;
	}
	
	public double getTime() {
		return time;
	}
	
	public void setTime(double time) {
		this.time = time;
	}
	
	public Interest(double principal, double rate, double time) {
		super();
		this.principal = principal;
		this.rate = rate;
		this.time = time;
	}
	
	public Interest() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	  public double simInt() {
		    return (principal * rate * time) / 100;
		  }
}
