<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ page import="org.assignment.Interest" %>  
<%
Interest interest = new Interest();
double p = Double.parseDouble(request.getParameter("principal"));
interest.setPrincipal(p);

double r = Double.parseDouble(request.getParameter("rate"));
interest.setRate(r);

double t = Double.parseDouble(request.getParameter("time"));
interest.setTime(t);


double simpleInterest = interest.simInt();
request.setAttribute("principal", p);
request.setAttribute("rate", r);
request.setAttribute("time", t);
request.setAttribute("result", simpleInterest);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Simple Interest</title>
<style>
body{
	background-color: black;
	color: white;
}
</style>
</head>
<body>
	<h1 style="text-align:center;">Result</h1>
	<br/>
	<p>Principal: ${principal }</p><br/>
	<p>Rate: ${rate}</p><br/>
	<p>Time: ${time}</p><br/>
	<h4>Simple Interest: ${result}</h4>
</body>
</html>