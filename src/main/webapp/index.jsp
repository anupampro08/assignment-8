<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Input Form</title>

<style>
td{
	padding:6px;
}
body{
	background-color: black;
	color: white;
}
</style>
</head>
<body>
<h2 style="text-align: center;">Simple Interest Calculator</h2>
  <form action="HelloServlet" method="post">
  	<table>
    <tr>
   	 <td>Principal:</td> 
   	 <td> <input type="text" name="principal" placeholder="Enter principle"/> </td>
    </tr>
    <tr>
   	 <td>Rate:</td> 
   	 <td> <input type="text" name="rate" placeholder="Enter rate"/> </td>
    </tr>
    <tr>
    </tr>
    <tr>
   	 <td>Time:</td> 
   	 <td> <input type="text" name="time" placeholder="Enter time"/> </td>
    </tr>
    <tr>
    	<td></td>
    	<td><input type="submit" value="Submit" /></td>
	</tr>
    </table>
  </form>
</body>
</html>